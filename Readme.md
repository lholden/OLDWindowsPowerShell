My personal PowerShell configuration. Feel free to use. ;)

![Screenshot](https://gitlab.com/lholden/WindowsPowerShell/raw/master/screenshot.png)

### Installation
This tested only on PowerShell 5.0. If you are not using Windows 10 and would like to upgrade to PowerShell 5.0, please download the ![Windows Management Framework 5.0](https://www.microsoft.com/en-us/download/details.aspx?id=50395)

This depends on the PowerShell Gallary. If you are having trouble installing packages, please see the PowerShell Gallary ![Getting Started](https://www.powershellgallery.com/GettingStarted?section=Get%20Started) page.

Clone this into your `%userprofile%\Documents` directory so that you have `%userprofile%\Documents\WindowsPowerShell` and launch PowerShell. :)

### Windows Shell color settings:
```
Screen Text: 8th
Screen Background: 1st
```

Colors:

Unix and Windows have their color orders mixed up. Where they differ in the following list, windows is first and unix is second.
```
00    Black         - 10 10 10    | #0a0a0a
01/04 Blue          - 87 120 193  | #5778c1
02    Green         - 150 169 103 | #96a967
03/06 Cyan          - 110 181 243 | #6eb5f3
04/01 Red           - 202 103 74  | #ca674a
05    Magenta       - 156 53 172  | #9c35ac
06/03 Yellow        - 211 169 74  | #d3a94a
07    Gray          - 207 207 207 | #cfcfcf
08    DarkGray      - 90 90 90    | #5a5a5a
09/12 Light Blue    - 111 155 202 | #6f9bca
10    Light Green   - 135 221 50  | #87dd32
11/14 Light Cyan    - 50 221 221  | #32dddd
12/09 Light Red     - 237 65 65   | #ed4141
13    Light Magenta - 169 124 164 | #a97ca4
14/11 Light Yellow  - 247 228 77  | #f7e44d
15    White         - 233 233 233 | #e9e9e9
```

If setting the colors by hand, the Colors tab has a bug where the next color you click on will get modified with the value from the field that is currently selected. If you click your current color after modifying it, then the next one... it should be fine.