$GitFound = $false
if (Get-Command git -ErrorAction SilentlyContinue) {
  $GitFound = $true
} 

if ($GitFound) {
  Assert-Module "posh-git"

  function Write-GitPrompt($Path) {
    if (Test-Path -Path (Join-Path $Path '.git') ) {
      Write-VcsStatus
      return
    }
    $SplitPath = Split-Path $Path
    if ($SplitPath) {
      Write-GitPrompt($SplitPath)
    }
  }

  Start-SshAgent -Quiet
}