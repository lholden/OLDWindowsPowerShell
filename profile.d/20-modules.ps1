Assert-Module PSReadLine
Assert-Module PSColor
Assert-Module Posh-Alias
Assert-Module Posh-SSH
Assert-Module TabExpansionPlusPlus

Remove-Item alias:gpv -Force
Assert-Module-Installed Pscx
Import-Module Pscx -arg "$PSLocalPath\Pscx.UserPreferences.ps1"

Import-Module Get-ChildItemColored
$global:lscolor_dir = "Blue"
